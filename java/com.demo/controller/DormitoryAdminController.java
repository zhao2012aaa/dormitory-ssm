package com.southwind.controller;

import com.southwind.entity.DormitoryAdmin;
import com.southwind.service.DormitoryAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

//控制器类`DormitoryAdminController`，用于处理与宿舍管理员相关的请求。

@Controller
@RequestMapping("/dormitoryAdmin")
public class DormitoryAdminController {

    @Autowired
    private DormitoryAdminService dormitoryAdminService;

//    `list()`: 该方法使用`@GetMapping("/list")`注解，处理GET请求，并返回一个`ModelAndView`对象。
    @GetMapping("/list")
    public ModelAndView list(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("adminmanager");
        modelAndView.addObject("list", this.dormitoryAdminService.list());
        return modelAndView;
    }

//    `search(String key, String value)`: 该方法使用`@PostMapping("/search")`注解，处理POST请求，并返回一个`ModelAndView`对象。
    @PostMapping("/search")
    public ModelAndView search(String key,String value){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("adminmanager");
        modelAndView.addObject("list", this.dormitoryAdminService.search(key, value));
        return modelAndView;
    }

//    `save(DormitoryAdmin dormitoryAdmin)`: 该方法使用`@PostMapping("/save")`注解，处理POST请求，并返回一个字符串。
    @PostMapping("/save")
    public String save(DormitoryAdmin dormitoryAdmin){
        this.dormitoryAdminService.save(dormitoryAdmin);
        return "redirect:/dormitoryAdmin/list";
    }

//    `delete(Integer id)`: 该方法使用`@PostMapping("/delete")`注解，处理POST请求，并返回一个字符串。
    @PostMapping("/delete")
    public String delete(Integer id){
        this.dormitoryAdminService.delete(id);
        return "redirect:/dormitoryAdmin/list";
    }

//    `update(DormitoryAdmin dormitoryAdmin)`: 该方法使用`@PostMapping("/update")`注解，处理POST请求，并返回一个字符串。
    @PostMapping("/update")
    public String update(DormitoryAdmin dormitoryAdmin){
        this.dormitoryAdminService.update(dormitoryAdmin);
        return "redirect:/dormitoryAdmin/list";
    }
}
